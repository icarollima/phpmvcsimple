<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';

$mail = new PHPMailer(true);     # Passing `true` enables exceptions
try {
    #Server settings
    $mail->SMTPDebug = 2;                   # Enable verbose debug output
    $mail->isSMTP(true);
    $mail->CharSet = 'UTF-8';               # Set mailer to use SMTP
    $mail->Host = '';                       # Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                 # Enable SMTP authentication
    $mail->Username = '';                   # SMTP username
    $mail->Password = '';                   # SMTP password
    #$mail->SMTPSecure = 'tls';             # Enable TLS encryption, `ssl` also accepted
    $mail->Port = 587;                      # TCP port to connect to

    # Recipients
    $mail->setFrom('no-reply@test.com', 'Support');
    $mail->addAddress('carollima.dev@gmail.com.br', 'Carol');     # Add a recipient # Name is optional
    #$mail->addReplyTo('info@example.com', 'Information');
    #$mail->addCC('cc@example.com');
    #$mail->addBCC('bcc@example.com');

    # Attachments
    #$mail->addAttachment('/var/tmp/file.tar.gz');         # Add attachments
    #$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    # Optional name

    # Content
    $mail->isHTML(true);                                  # Set email format to HTML
    $mail->Subject = 'Here is the subject';
    $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
    echo 'Message has been sent';
} catch (Exception $e) {
    echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
}