<?php

# Application base directory
define('BASE_PATH', dirname(__FILE__));

# MySQL access credentials
/*define('MYSQL_HOST', 'localhost');
define('MYSQL_USER', 'usuario');
define('MYSQL_PASS', 'senha');
define('MYSQL_DBNAME', 'nome_do_banco');*/

# PHP settings
ini_set('display_errors', true);
error_reporting(E_ALL);
date_default_timezone_set('America/Sao_Paulo');