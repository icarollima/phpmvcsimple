<?php

# Includes Composer autoloader
require_once './vendor/autoload.php';
require 'init.php';

# Instantiate Slim, enabling the errors (useful for debug, in development). Pattern: $app = new Slim\App();
$app = new \Slim\App([ 'settings' => ['displayErrorDetails' => true]]);

$route = $app->getContainer()->get('request')->getUri()->getPath();

# Home page
$app->get('/', function(){

    $UserController = new \app\controller\UserController;
    $UserController->list();
});

$app->post('/search/{id}', function($request){

    $id = $request->getAttribute('id');
    $UserController = new \app\controller\UserController;
    $UserController->search($id);
});

# Process the registration form
$app->post('/add', function($request){

    $user = json_decode($request->getBody('user'));
    $UserController = new \app\controller\UserController;
    $UserController->store($user);

});

# User Addition. Displays the registration form
$app->get('/create', function ()
{
    $UserController = new \app\controller\UserController;
    $UserController->create();
});

/*
$app->get('/edit/{id}', function ($request){

    $id = $request->getAttribute('id');
    $UsersController = new \App\Controllers\UsersController;
    $UsersController->edit($id);
});

$app->post('/edit', function (){

    $UsersController = new \App\Controllers\UsersController;
    $UsersController->update();
});

$app->get('/remove/{id}', function ($request){

    $id = $request->getAttribute('id');
    $UsersController = new \App\Controllers\UsersController;
    $UsersController->remove($id);
});*/

$app->run();