<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>MVC Registration System</title>
    <link rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css">
</head>
<body>
    <?php if (isset($viewName)) { $path = viewsPath() . $viewName . '.php'; if (file_exists($path)) { require_once $path; } } ?>
</body>
</html>
