<div class="container">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="glyphicon glyphicon-list"></i> Registration </h3>
        </div>
        <div class="panel-body">
            <form action="/add" method="post">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">Name: </label>
                            <input class="form-control input-sm" type="text" name="name" id="name">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="email">Email: </label>
                            <input class="form-control input-sm" type="text" name="email" id="email">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            Gênero:
                            <input type="radio" name="gender" id="gener_m" value="m">
                            <label for="gener_m">Male </label>
                            <input type="radio" name="gender" id="gener_f" value="f">
                            <label for="gener_f">Female </label>
                        </div>
                    </div>
                </div>
                <!--<div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="birthdate">Birthdate: </label>
                            <input class="form-control input-sm" type="text" name="birthdate" id="birthdate" placeholder="dd/mm/YYYY">
                        </div>
                    </div>
                </div>-->
                <div class="row">
                    <div class="col-md-12 " >
                        <input  type="submit" class="btn btn-success" value="Register">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>