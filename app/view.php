<?php

namespace app;

/**
 * Class to generate an application view.
 * It is responsible for loading the general template and adding the view that should be displayed
 */
class view
{
    /**
     * Displays a view.
     * Function inspired by the Laravel 4. http://laravel.com/docs/4.2/responses#views
     *
     * @link http://laravel.com/docs/4.2/responses#views
     * @param  string $viewName   View name, which is the name of the file in lib / views, without the ".php"
     * @param  array  $customVars (optional) Array with variables that will be used in the view
     */
    public static function make($viewName, array $customVars = array())
    {
        # Create $customVars array variables
        extract($customVars);

        # It includes the template, which will render the view in the variable $viewName
        require_once viewsPath() . 'template.php';
    }
}
