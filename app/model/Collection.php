<?php

namespace app\model;

class Collection{
    private $items = array();

    public function addItem($obj, $key = null) {
        if ($key == null) {
            $this->items[] = $obj;
        }
        else {
            if (isset($this->items[$key])) {
                throw new KeyHasUseException("Key $key already in use.");
            }
            else {
                $this->items[$key] = $obj;
            }
        }
    }

    public function deleteItem($key) {
        if (isset($this->items[$key])) {
            unset($this->items[$key]);
    }
        else {
            throw new KeyInvalidException("Invalid key $key.");
        }
    }

    public function getItem($key) {
        if (isset($this->items[$key])) {
            return $this->items[$key];
        }
        else {
            throw new KeyInvalidException("Invalid key $key.");
        }
    }

    # Adding a method that can provide a list of keys to any external code that might need.
    public function keys() {
        return array_keys($this->items);
    }

    # It might also be helpful to know how many items are in the collection.
    public function length() {
        return count($this->items);
    }

    # Whether a given key exists in the collection
    public function keyExists($key) {
        return isset($this->items[$key]);
    }
}