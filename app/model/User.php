<?php

namespace app\model;

class User{

    private $id;
    private $nome;

    public function getId(){return $this->id;}
    public function setId($id){$this->id = $id;}
    public function getNome(){return $this->nome;}
    public function setNome($nome){$this->nome = $nome;}

    # It works by passing a stdClass object or an array with the same fields as the class
    public function __construct($attributes){

        foreach ($attributes as $name => $value) {
            $this->{$name} = $value;
        }
    }

    # Simple test
    public static function selectAll() {
        $users = array("Carol", "Ed", "Fellipe");
        return $users;
    }

}