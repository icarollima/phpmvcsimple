<?php
namespace app\controller;
use app\model\User;

class UserController{

    public function list(){
        $user = User::selectAll();
        print_r($user);
    }

    public function search($id){
        print_r('Id = ' . $id);
    }

    public function store($user){
        try{
            $usuario = new User($user->user);
            print_r($usuario);
            $json['erro'] = 0;
            $json['msg'] = 'Dados gravados com sucesso.';
        }catch (Exception $e){
            $json['erro'] = $e->getCode();
            $json['msg'] = $e->getMessage();
        }
        return json_encode($json);
    }

    public function create(){
         \app\view::make('user.create');
    }

}